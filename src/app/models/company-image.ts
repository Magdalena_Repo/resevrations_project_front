export class CompanyImage {
  id: number;
  imageUrl: string;

  constructor(imageUrl: string) {
    this.imageUrl = imageUrl;
  }

}
