import {User} from './user';
import {Company} from './company';

export class Reservation {
  id: number
  remark: String;
  personCount: number;
  forDate: any;
  userId: number;
  companyId: number;


  constructor(remark: String, personCount: number, date: Date,  companyId: number) {
    this.remark = remark;
    this.personCount = personCount;
    this.forDate = date;
    this.companyId = companyId;
  }
}
